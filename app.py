#-*- coding: utf-8 -*-
import sys
import logging

from flask import Flask, render_template, request
from chatbot.model.Person import *
from chatbot.model.Story import *

app = Flask(__name__)

stories = [
    Story("Jak było na wojnie", "Jak było na wojnie? Na wojnie było ciężko, nie dało się żyć"), 
    Story("Kto mowi", "Podczas powastania bylam sanitariuszka."), 
    Story("Czołg na starym mieście", "Z tym czołgiem to było nastęująco...")
]

person = Person(stories, ["Nie slysze Ciebie skarbie", "Eee?", "No masz Ci los, znow przerywa..."])    


@app.route('/', methods=['GET', 'POST'])
def main(name=None):
    errors = []
    answer = None
    if request.method == "POST":
        try:
            question = request.form['question']
            answer = person.ask(question)
        except:
            errors.append(sys.exc_info())
    return render_template('index.html', errors=errors, answer=answer, stories=stories)

if __name__ == '__main__':
    app.run(debug=True)
