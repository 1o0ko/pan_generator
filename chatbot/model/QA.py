import chardet  
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

def make_it_nice(string):
    #return string.decode(chardet.detect(string)['encoding']) 
    return string

class QA:        
    def __init__(self, person):    
        self.person =  person        
        self.stories = person.stories
        self.tfidf = TfidfVectorizer()
        self.tfidf.fit([s.abstract for s in self.stories])               
        
    def answer(self, question, stories, debug = False):        
        q_vec = self.tfidf.transform([make_it_nice(question)])                
        s_vec = self.tfidf.transform([s.abstract for s in self.stories])                            

        similarities = cosine_similarity(q_vec, s_vec)
        
        if not similarities.any():
            return self.person.prompt()
        
        idx = similarities.argmax()
        
        if debug:
            print cosine_similarity(q_vec, s_vec)
            print cosine_similarity(q_vec, s_vec).argmax()
        
        if debug:        
            for s in stories:
                print s.abstract, s.text                
                print cosine_similarity(q_vec, self.tfidf.transform([s.abstract]))                
        
        return stories[idx].text                                