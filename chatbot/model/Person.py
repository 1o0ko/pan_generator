import random
from QA import QA

class Person:        
    def __init__(self, stories, prompts = ["ee?"]):
        self.stories = stories
        self.qa = QA(self)
        self.prompts = prompts
    
    def addStory(self, story):
        self.stories.append(story)
        
    def ask(self, question, debug = False):
        return self.qa.answer(question, self.stories, debug)        
    
    def prompt(self):
        return random.choice(self.prompts)