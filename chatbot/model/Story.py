from chatbot.utils.string import make_it_nice

class Story:
    def __init__(self, abstract, text):
        self.abstract = make_it_nice(abstract)
        self.text = make_it_nice(text)