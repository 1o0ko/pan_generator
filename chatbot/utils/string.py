import chardet

def make_it_nice(string):
    return string.decode(chardet.detect(string)['encoding']) 